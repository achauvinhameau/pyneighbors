# server.py
# -*- Mode: Python; python-indent-offset: 4 -*-
#
# Time-stamp: <2019-08-24 18:15:41 alex>

import socket
import sys
import logging
import json
import time
import signal
import traceback
import threading

import ipConf

LOGFORMAT = '%(asctime)-15s [%(levelname)s] %(filename)s:%(lineno)d - %(message)s'
logging.basicConfig(format=LOGFORMAT, level=logging.DEBUG)

ip = ipConf.ipConf()

if not ip.hasDefaultRoute():
    logging.abort("no default route found")
    exit()

identity = {
    "link": ip.getLinkAddr(),
    "interface": ip.getIfName(),
    "ipv4": ip.getIfIPv4(),
    "ipv6": ip.getIfIPv6(),
    "time": int(time.time()),
}

dest = ('<broadcast>',10100)

# -----------------------------------------
def trap_signal(sig, _):
    """ trap all signals for stop """

    exit()


# -----------------------------------------
def send_message():
    while True:
        # logging.debug("send message")
        identity['time'] = int(time.time())
        msg = bytes(json.dumps(identity), 'UTF-8')

        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        s.sendto(msg, dest)

        time.sleep(5)

# -----------------------------------------
def write_neighbors_file(db):
    with open("neighbors.txt", "w") as f: 
        for ip in db:
            f.write(db[ip]['ipv4']+"\n")

# -----------------------------------------
def read_message(myip):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    s.bind(('', 10100))
    s.settimeout(5)
    
    aipdb = {}

    while True:
        # logging.debug("read message")
        b_has_changed = False
        try:
            message, address = s.recvfrom(10104)
            msg = json.loads(message)

            if msg['ipv4'] not in myip:
                if msg['ipv4'] not in aipdb:
                    b_has_changed = True
                aipdb[msg['ipv4']] = { 'last': time.time(),
                                      'ipv4': msg['ipv4'],
                                      'ipv6': msg['ipv6'],
                                      'interface': msg['interface']
                                  }
        except socket.timeout:
            logging.debug("no neighbor")
        except:
            traceback.print_exc()

        for ip in aipdb:
            if time.time() - aipdb[ip]['last'] > 10:
                del aipdb[ip]
                b_has_changed = True
                break

        if b_has_changed:
            print(aipdb)
            write_neighbors_file(aipdb)

# -----------------------------------------
signal.signal(signal.SIGTERM, trap_signal)
signal.signal(signal.SIGINT, trap_signal)

tsend = threading.Thread(target=send_message)
tread = threading.Thread(target=read_message, args=(identity['ipv4'],))

tread.start()
tsend.start()
